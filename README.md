# 前言
### 答疑解惑
```html
提问1: 开源的代码生成器已经烂大街了，你为什么还要写？比别人的要厉害好用么？
答：确实现在生成器已经遍地都是，但ddd的落地确实很少，我写的主要目的是为了我后面要做的东西而准备，
可以节省很多时间，一键生成CRUD,同时提供部分utils类型，直接启动就可以使用，于是开源共享。
（非常欢迎大佬一起迭代，未来让更多的人能够快乐的摸鱼）

提问2: 什么是DDD?
答：相信大家都听过DDD，翻译过来就是“滴答滴”,著名歌手coco李玟曾经唱过倒数开始 Di Da Di
Di da di da di da.....（深井冰犯了，不好意思啊）
DDD是一种思想，详情咨询度娘，注意一点，DDD适用于中大型平台，小型的三层框架就很nice

提问3：生成的代码提供了哪些东西？
答：
1、集成mbp,生成crud各层代码，分页支持
2、解决单体跨域配置（cloud走gateway）
3、集成knife4j，生成接口文档
4、集成druid监控管理，打印所有执行sql
5、集成AOP，统一日志拦截（按需扩展）
6、支持公共结构体封装，直接使用
7、库表系统字段自赋值
8、提供dockerfile

提问4：还有什么要说的吗？
答：没了
```

# 工程简介

名称： ddd-easy-generator v1.0 (DDD代码生成器)
```
.
└── src
├── main
│   ├── java
│   │   └── com
│   │       └── easy
│   │           └── generator
│   │               ├── application
│   │               │   ├── constant
│   │               │   └── service
│   │               ├── domain
│   │               │   ├── entity
│   │               │   │   └── enums
│   │               │   └── repository
│   │               ├── infrastructure
│   │               │   ├── config
│   │               │   ├── mapper
│   │               │   ├── service
│   │               │   │   └── impl
│   │               │   └── utils
│   │               └── interfaces
│   │                   ├── controller
│   │                   └── dto
│   └── resources
│       ├── mapper
│       ├── static
│       └── templates
└── test
└── java
└── com
└── easy
└── generator
```

# 使用说明
1. ***怎么用？***
* 时间问题，此版本先以UT的方式{@link com.easy.generator.EasyGeneratorBootApplicationTests}
* 按需修改参数值即可，然后小手轻轻的那么一点，代码生成完毕

2. ***需要修改什么？***
* application.yml文件的库名，用户名,密码
* {@link com.easy.generator.infrastructure.service.impl.EasyGeneratorRepositoryImpl 代码行数：69}，配置单表生成，默认生成该库下所有表

3. ***如果项目生成后，启动提示找不到application.yml***
* 解决办法：resources右键 ——>Mark Directory as ->Resources Root

# 参考文档

* http://freemarker.foofun.cn/
* https://github.com/alibaba/druid
* https://hutool.cn/docs/#/
* https://baomidou.com/

# 集成

1. druid 监控页面，所有的sql默认都会记录到监控页面，控制台无log
* http://localhost:5505/druid/index.html

2. knife4j 生成接口文档 {@link https://doc.xiaominfo.com/}
* http://localhost:5505/doc.html#/home


# 版本迭代计划
1. 可视化管理页面
2. 集成常用二方包
3. Cloud版本，集成nacos、gateway
4. 其他数据库的兼容
5. 由各位需求着来定（欢迎留言提需求，更欢迎提PR一起迭代）