package com.easy.generator;

import com.easy.generator.domain.repository.IEasyGeneratorDomainRepository;
import freemarker.template.TemplateException;
import org.apache.commons.lang.time.DateFormatUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class EasyGeneratorBootApplicationTests {

    @Autowired
    private IEasyGeneratorDomainRepository codeGeneratorService;

    @Test
    void contextLoads() throws TemplateException, IOException {
        Map<String,Object> objectMap = new HashMap<>(8);
        objectMap.put("projectRootDir","/Users/xiaobei/Desktop/aps/");
        objectMap.put("groupId","com.five");
        objectMap.put("artifactId","five-team-oa-boot");
        objectMap.put("name","five-team-oa-boot");
        objectMap.put("description","12");
        objectMap.put("packageName","com.five");
        objectMap.put("jdkVersion","11");
        objectMap.put("author","smallNorth_Lee");
        objectMap.put("date", DateFormatUtils.format(new Date(), "yyyy/MM/dd"));
        codeGeneratorService.generator(objectMap);

    }

}
