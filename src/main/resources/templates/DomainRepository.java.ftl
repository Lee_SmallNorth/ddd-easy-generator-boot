package ${package}.domain.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import ${package}.domain.repository.po.${className}PO;
import ${package}.interfaces.dto.${className}QueryDTO;
import ${package}.interfaces.vo.${className}VO;

/**
 * ${className}服务类
 *
 * @author ${author}
 * @date ${date}
 */
public interface I${className}DomainRepository extends IService<${className}PO>{

    /**
     * 分页查询
     *
     * @param page     分页参数
     * @param queryDTO 业务参数
     * @return 分页
     */
    IPage<${className}VO> queryPage(IPage<${className}VO> page, ${className}QueryDTO queryDTO);

}