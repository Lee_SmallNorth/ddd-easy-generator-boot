package ${package}.interfaces.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${package}.infrastructure.utils.Result;
import ${package}.interfaces.dto.${className}QueryDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import lombok.AllArgsConstructor;
import ${package}.application.service.${className}AppService;
import ${package}.interfaces.vo.${className}VO;
import ${package}.interfaces.dto.${className}DTO;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Api(tags = "${comments}")
@RestController
@AllArgsConstructor
@RequestMapping("/${apiName}")
public class ${className}Controller {

    private final ${className}AppService ${tableAliasName}AppService;

    @ApiOperation(value = "列表分页查询")
    @PostMapping("/list")
    public Result<IPage<${className}VO>> queryPage(@RequestBody ${className}QueryDTO queryDTO) {
        return Result.ok(${tableAliasName}AppService.queryPage(queryDTO));
    }

    @ApiOperation(value = "编辑/保存")
    @PostMapping
    public Result<Boolean> save(@RequestBody ${className}DTO ${tableAliasName}DTO) {
        return Result.ok(${tableAliasName}AppService.save(${tableAliasName}DTO));
    }

    @ApiOperation(value = "查询数据明细")
    @GetMapping("/{id}")
    public Result<${className}VO> detail(@PathVariable Long id) {
        return Result.ok(${tableAliasName}AppService.detail(id));
    }

   @ApiOperation(value = "根据id删除数据")
   @DeleteMapping("/{id}")
   public Result<Boolean> deleteById(@PathVariable Long id) {
       return Result.ok(${tableAliasName}AppService.deleteById(id));
   }
}