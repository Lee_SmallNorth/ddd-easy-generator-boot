package ${package}.infrastructure.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 分页查询公有类
 *
 * @author ${author}
 * @date ${date}
 */
@Data
@ApiModel(description = "返回体结构")
public class Query {

    @ApiModelProperty(value = "当前页")
    private Integer current;

    @ApiModelProperty(value = "条数")
    private Integer size;

    @ApiModelProperty(value = "关键字")
    private String keyWord;
}