
<#--自行修改私有仓库jdk-->
FROM java:8

ADD target/${artifactId}.jar app.jar

RUN bash -c 'touch /app.jar'

RUN /bin/cp /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo 'Asia/Shanghai' >/etc/timezone

ENTRYPOINT java -jar /app.jar
