package ${package}.infrastructure.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import ${package}.domain.repository.po.${className}PO;
import ${package}.interfaces.dto.${className}QueryDTO;
import ${package}.interfaces.vo.${className}VO;

/**
 * <p>
 * ${comments}Mapper
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
public interface ${className}Mapper extends BaseMapper<${className}PO> {

    /**
     * 分页查询
     *
     * @param page     分页参数
     * @param queryDTO 业务参数
     * @return 分页
     */
    IPage<${className}VO> queryPage(IPage<${className}VO> page, ${className}QueryDTO queryDTO);

}