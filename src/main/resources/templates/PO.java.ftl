package ${package}.domain.repository.po;

import com.baomidou.mybatisplus.annotation.TableName;
import ${package}.infrastructure.utils.mp.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
<#if hasLocalDate>
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
</#if>
<#if hasLocalDateTime>
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
</#if>

/**
 * <p>
 * ${comments}
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("${tableName}")
public class ${className}PO extends BaseEntity {

    <#list columns as column>
    /**
     * ${column.columnComment}
     */
    <#if column.dataType == "LocalDate">
    @JsonFormat(pattern = "yyyy-MM-dd")
    </#if>
    <#if column.dataType == "LocalDateTime">
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    </#if>
    private ${column.dataType} ${column.columnName};
    </#list>

}