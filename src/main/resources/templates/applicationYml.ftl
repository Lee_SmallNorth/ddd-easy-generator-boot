server:
  port: 5505

spring:
  application:
    name: ${artifactId}
  datasource:
    type: com.alibaba.druid.pool.DruidDataSource
    druid:
      driver-class-name: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://localhost:3306/${artifactId}?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai
      username: root
      password: root
      max-wait: 20
      initial-size: 5
      max-active: 6000
      min-idle: 10
      validation-query: select 1
      # druid 可视化
      stat-view-servlet:
        enabled: true
        url-pattern: /druid/*
        login-username: admin
        login-password: admin
      web-stat-filter:
        enabled: true
        url-pattern: /*
        exclusions: "*.js,*.gif,*.jpg,*.png,*.css,*.ico,/druid/*"
        session-stat-max-count: 1000
        # 监控单个url调用的sql列表
        profile-enable: true
      filters: stat,wall
