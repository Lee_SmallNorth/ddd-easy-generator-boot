package ${package}.domain.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ${package}.application.assembler.${className}Assembler;
import ${package}.domain.entity.${className};
import ${package}.domain.repository.I${className}DomainRepository;
import ${package}.interfaces.dto.${className}QueryDTO;
import ${package}.interfaces.vo.${className}VO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * ${className}逻辑处理层
 *
 * @author ${author}
 * @date ${date}
 */
@Service
@AllArgsConstructor
public class ${className}DomainService {

    private final I${className}DomainRepository ${tableAliasName}DomainRepository;


    /**
     * 分页查询
     *
     * @param page     分页参数
     * @param queryDTO 业务参数
     * @return 分页列表
     */
    public IPage<${className}VO> queryPage(IPage<${className}VO> page, ${className}QueryDTO queryDTO) {
        return ${tableAliasName}DomainRepository.queryPage(page,queryDTO);
    }

    /**
     * 编辑/保存
     *
     * @param ${tableAliasName} 参数
     * @return true/false
     */
    public Boolean save(${className} ${tableAliasName}) {
        return ${tableAliasName}DomainRepository.saveOrUpdate(${className}Assembler.build().toEntityPO(${tableAliasName}));
    }

    /**
     * 查询数据明细
     *
     * @param id 主键
     * @return vo
     */
    public ${className} getId(Long id) {
        return ${className}Assembler.build().toEntity(${tableAliasName}DomainRepository.getById(id));
    }

    /**
     * 根据id删除一条数据
     *
     * @param id 主键
     * @return true/false
     */
    public Boolean deleteById(Long id) {
        return ${tableAliasName}DomainRepository.removeById(id);
    }
}