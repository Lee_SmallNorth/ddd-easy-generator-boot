package ${package}.interfaces.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
<#if hasLocalDate>
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
</#if>
<#if hasLocalDateTime>
import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDateTime;
</#if>
import java.io.Serializable;

/**
 * <p>
 * ${comments}
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
@Data
@ApiModel(value = "${className}VO对象", description = "${comments}")
public class ${className}VO implements Serializable {

    private static final long serialVersionUID = 1L;

    <#list columns as column>
    @ApiModelProperty(value = "${column.columnComment}")
    <#if column.dataType == "LocalDate">
    @JsonFormat(pattern = "yyyy-MM-dd")
    </#if>
    <#if column.dataType == "LocalDateTime">
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    </#if>
    private ${column.dataType} ${column.columnName};

    </#list>

}