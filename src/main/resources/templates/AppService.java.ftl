package ${package}.application.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package}.application.assembler.${className}Assembler;
import ${package}.domain.service.${className}DomainService;
import ${package}.interfaces.dto.${className}DTO;
import ${package}.interfaces.dto.${className}QueryDTO;
import ${package}.interfaces.vo.${className}VO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * ${className}应用层服务
 *
 * @author ${author}
 * @date ${date}
 */
@Service
@AllArgsConstructor
public class ${className}AppService {

    public final ${className}DomainService ${tableAliasName}DomainService;

    /**
     * 列表分页查询
     *
     * @param queryDTO 参数
     * @return 分页列表
     */
    public IPage<${className}VO> queryPage(${className}QueryDTO queryDTO) {
        IPage<${className}VO> page = new Page<>(queryDTO.getCurrent(),queryDTO.getSize());
        return ${tableAliasName}DomainService.queryPage(page,queryDTO);
    }

    /**
     * 编辑/保存
     *
     * @param ${tableAliasName}DTO 参数
     * @return true/false
     */
    public Boolean save(${className}DTO ${tableAliasName}DTO) {
        return ${tableAliasName}DomainService.save(${className}Assembler.build().toEntity(${tableAliasName}DTO));
    }

    /**
     * 根据id查询数据明细
     *
     * @param id 主键
     * @return vo
     */
    public ${className}VO detail(Long id) {
        if(StringUtils.isEmpty(id)){
            return new ${className}VO();
        }
        ${className}VO ${tableAliasName}VO = ${className}Assembler.build().toEntityVo(${tableAliasName}DomainService.getId(id));
        return ${tableAliasName}VO;
    }

    /**
     * 根据id删除一条数据,逻辑删除 is_deleted = 1
     *
     * @param id 主键
     * @return true/false
     */
    public Boolean deleteById(Long id) {
        if(StringUtils.isEmpty(id)){
            return Boolean.FALSE;
        }
        return ${tableAliasName}DomainService.deleteById(id);
    }
}