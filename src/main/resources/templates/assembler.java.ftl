package ${package}.application.assembler;

import ${package}.domain.entity.${className};
import ${package}.infrastructure.utils.BeanUtil;
import ${package}.domain.repository.po.${className}PO;
import ${package}.interfaces.dto.${className}DTO;
import ${package}.interfaces.vo.${className}VO;

/**
 * ${className}转换器
 *
 * @author ${author}
 * @date ${date}
 */
public class ${className}Assembler {

    public static ${className}Assembler build() {
        return new ${className}Assembler();
    }

    public ${className}VO toEntityVo(${className} ${tableAliasName}) {
        return BeanUtil.copy(${tableAliasName}, ${className}VO.class);
    }

    public ${className} toEntity(${className}VO ${tableAliasName}VO) {
        return BeanUtil.copy(${tableAliasName}VO, ${className}.class);
    }

    public ${className} toEntity(${className}DTO ${tableAliasName}DTO) {
        return BeanUtil.copy(${tableAliasName}DTO, ${className}.class);
    }

    public ${className} toEntity(${className}PO ${tableAliasName}PO) {
        return BeanUtil.copy(${tableAliasName}PO, ${className}.class);
    }

    public ${className}PO toEntityPO(${className} ${tableAliasName}) {
        return BeanUtil.copy(${tableAliasName}, ${className}PO.class);
    }
}