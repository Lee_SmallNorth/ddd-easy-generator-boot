package ${package}.interfaces.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

<#if hasBigDecimal>
import java.math.BigDecimal;
</#if>
<#if hasLocalDate>
import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonFormat;
</#if>
<#if hasLocalDateTime>
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;
</#if>
import java.io.Serializable;

/**
 * <p>
 * ${comments}
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
@Data
@ApiModel(value = "${className}DTO对象", description = "${comments}")
public class ${className}DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    <#list columns as column>
    @ApiModelProperty(value = "${column.columnComment}")
    <#if column.dataType == "LocalDate">
    @JsonFormat(pattern = "yyyy-MM-dd")
    </#if>
    <#if column.dataType == "LocalDateTime">
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    </#if>
    private ${column.dataType} ${column.columnName};

    </#list>

}