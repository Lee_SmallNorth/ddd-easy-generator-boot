package ${package}.domain.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 枚举模版
 *
 * @author ${author}
 * @date ${date}
 */
@Getter
@AllArgsConstructor
public enum TemplateEnum {

    /**
     * 金额类型
     */
    BIG_DECIMAL("BigDecimal", "金额"),

    /**
     * 日期类型
     */
    LOCAL_DATE("LocalDate", "日期类型"),

    /**
     * 时间类型
     */
    LOCAL_DATE_TIME("LocalDateTime", "时间类型");

    /**
     * 字段类型
     */
    private final String columnType;

    /**
     * 字段描述
     */
    private final String columnDesc;

    /**
     * 值对象，如果业务场景需获取枚举name或者code,应在entity中配合使用，以整体为基准规范，
     * 可参考 @link{TableColumnDO} setDataType()方法，具体使用类GenerationJavaClassImpl 代码行数：81
     *
     * @param columnType
     * @return
     */
    public static String ofColumnType(String columnType) {
        TemplateEnum columnTypeEnum = Arrays.stream(TemplateEnum.values())
                .filter(x -> x.getColumnType().equals(columnType)).findFirst().orElse(null);
        if (null != columnTypeEnum) {
            return columnTypeEnum.getColumnType();
        }
        return null;
    }

}