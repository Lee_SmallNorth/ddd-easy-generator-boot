package ${package}.interfaces.dto;

import ${package}.infrastructure.utils.Query;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * ${comments}分页参数
 * </p>
 *
 * @author ${author}
 * @date ${date}
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "${className}QueryDTO对象", description = "${comments}")
public class ${className}QueryDTO extends Query {

//    @ApiModelProperty(value = "样例")
//    private String simpleName;

}