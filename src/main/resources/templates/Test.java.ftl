package ${package};

import org.springframework.boot.test.context.SpringBootTest;
import org.junit.jupiter.api.Test;

/**
 * 测试类
 *
 * @author ${author}
 * @date ${date}
 */
@SpringBootTest
class ${className}Tests{

    @Test
    void test() {

    }

}