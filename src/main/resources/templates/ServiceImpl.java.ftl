package ${package}.infrastructure.repository.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ${package}.domain.repository.I${className}DomainRepository;
import ${package}.domain.repository.po.${className}PO;
import ${package}.interfaces.dto.${className}QueryDTO;
import ${package}.interfaces.vo.${className}VO;
import ${package}.infrastructure.repository.mapper.${className}Mapper;
import org.springframework.stereotype.Service;

/**
 * ${className}服务实现类
 *
 * @author ${author}
 * @date ${date}
 */
@Service
public class ${className}ServiceImpl extends ServiceImpl<${className}Mapper,${className}PO> implements I${className}DomainRepository{

    @Override
    public IPage<${className}VO> queryPage(IPage<${className}VO> page, ${className}QueryDTO queryDTO) {
      return this.baseMapper.queryPage(page, queryDTO);
    }

}