package ${package}.infrastructure.utils;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

/**
 * 返回体
 *
 * @author ${author}
 * @date ${date}
 */
@Data
@ApiModel(description = "返回体结构")
public class Result<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "返回码")
    private int code;

    @ApiModelProperty(value = "返回信息")
    private String msg;

    @ApiModelProperty(value = "返回数据")
    private T data;

    @ApiModelProperty(value = "请求时间戳")
    private Long timestamp;

    public Result() {
        super();
    }

    public Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
    * 成功
    *
    * @param data 返回值
    * @param <T>  泛型
        * @return Result
        */
    public static <T> Result<T> ok(T data) {
        return restResult(HttpStatus.OK.value(), "执行成功！", data);
    }

    /**
    * 成功
    *
    * @param data 返回值
    * @param msg  消息
    * @param <T>  泛型
    * @return Result
    */
    public static <T> Result<T> ok(String msg, T data) {
        return restResult(HttpStatus.OK.value(), msg, data);
    }

   /**
   * 错误返回
   *
   * @param msg 消息
   * @param <T> 泛型
   * @return Result
   */
   public static <T> Result<T> error(String msg) {
       return restResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), msg, null);
   }

   /**
   * 返回
   *
   * @param data 数据
   * @param code 错误码
   * @param msg  消息
   * @param <T>  泛型
   * @return
   */
   private static <T> Result<T> restResult(int code, String msg, T data) {
       Result<T> result = new Result<>();
       result.setCode(code);
       result.setData(data);
       result.setMsg(msg);
       result.setTimestamp(System.currentTimeMillis());
       return result;
   }
}


