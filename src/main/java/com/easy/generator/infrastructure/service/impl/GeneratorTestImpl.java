package com.easy.generator.infrastructure.service.impl;

import com.easy.generator.infrastructure.service.BaseService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

/**
 * 生成测试类
 *
 * @author smallNorth_Lee
 * @date 2022/1/11
 */
@Service
public class GeneratorTestImpl extends BaseService {

    /**
     * 生成测试类
     *
     * @param ftlMap          变量值
     * @param projectRootPath 项目文件路径
     */
    public void doGeneration(Map<String, Object> ftlMap, String projectRootPath) {
        // 生成测试类
        String className = ftlMap.get("className") + "Tests";
        super.writeFile(new File(projectRootPath, className + ".java"), "Test.java.ftl", ftlMap);
    }
}
