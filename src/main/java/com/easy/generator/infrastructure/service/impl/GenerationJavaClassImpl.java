package com.easy.generator.infrastructure.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.easy.generator.application.constant.AppConstant;
import com.easy.generator.domain.entity.TableColumnDO;
import com.easy.generator.domain.entity.TableInfoDO;
import com.easy.generator.domain.entity.enums.ColumnTypeEnum;
import com.easy.generator.domain.repository.ITableColumnDomainRepository;
import com.easy.generator.domain.repository.ITableInfoDomainRepository;
import com.easy.generator.infrastructure.service.BaseService;
import com.easy.generator.infrastructure.utils.ConvertColumnClassUtils;
import com.easy.generator.infrastructure.utils.ConvertTableClassUtils;
import com.easy.generator.interfaces.dto.TableInfoPageDTO;
import lombok.AllArgsConstructor;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * com.five.generator.infrastructure.service.impl
 *
 * @author smallNorth_Lee
 * @date 2022/1/14
 */
@Service
@AllArgsConstructor
public class GenerationJavaClassImpl extends BaseService {

    private final ITableColumnDomainRepository tableColumnDomainService;

    private final ITableInfoDomainRepository tableInfoDomainService;

    private static final Configuration config = getConfig();

    /**
     * @param objectMap       项目配置信息
     * @param projectRootPath 项目文件路径
     * @param tableName       表名
     */
    public void doGeneration(Map<String, Object> objectMap, String projectRootPath, String tableName) {

        //生成该库下所有表的代码
        if (StringUtils.isEmpty(tableName)) {
            TableInfoPageDTO tableInfoPageDTO = new TableInfoPageDTO();
            tableInfoPageDTO.setSize(1000);
            tableInfoPageDTO.setCurrent(1);
            IPage<TableInfoDO> tableInfoDOIPage = tableInfoDomainService.queryTables(tableInfoPageDTO);
            List<TableInfoDO> records = tableInfoDOIPage.getRecords();
            records.forEach(tableInfoDO -> {
                String className = ConvertTableClassUtils.getClassName(tableInfoDO.getTableName());
                createCode(projectRootPath, className, convertDataModel(objectMap, tableInfoDO.getTableName(), className));
            });
        } else {
            String[] tableNames = tableName.split(",");
            for (String name : tableNames) {
                String className = ConvertTableClassUtils.getClassName(name);
                createCode(projectRootPath, className, convertDataModel(objectMap, name, className));
            }
        }
    }

    /**
     * 封装dataModel
     *
     * @param tableName 表名
     */
    private Map<String, Object> convertDataModel(Map<String, Object> objectMap, String tableName, String className) {
        List<TableColumnDO> tableColumns = tableColumnDomainService.queryTableColumn(tableName);
        //公共字段不生成,如需要手动添加
        List<TableColumnDO> resTableColumn = new ArrayList<>();
        AtomicBoolean hasBigDecimal = new AtomicBoolean(false);
        AtomicBoolean hasLocalDate = new AtomicBoolean(false);
        AtomicBoolean hasLocalDateTime = new AtomicBoolean(false);
        tableColumns.forEach(tableColumnDO -> {
            if (!AppConstant.COMMON_FIELD.contains(tableColumnDO.getColumnName())) {
                String columnType = config.getString(tableColumnDO.getDataType());
                if (!hasBigDecimal.get() && ColumnTypeEnum.BIG_DECIMAL.getColumnType().equals(columnType)) {
                    hasBigDecimal.set(true);
                }
                if (!hasLocalDate.get() && ColumnTypeEnum.LOCAL_DATE.getColumnType().equals(columnType)) {
                    hasLocalDate.set(true);
                }
                if (!hasLocalDateTime.get() && ColumnTypeEnum.LOCAL_DATE_TIME.getColumnType().equals(columnType)) {
                    hasLocalDateTime.set(true);
                }
                tableColumnDO.setDataType(config.getString(tableColumnDO.getDataType()));
                tableColumnDO.setColumnName(ConvertColumnClassUtils.getColumnName(tableColumnDO.getColumnName()));
                resTableColumn.add(tableColumnDO);
            }
        });
        objectMap.put("className", className);
        TableInfoDO tableInfoDO = tableInfoDomainService.queryTableInfo(tableName);
        objectMap.put("comments", tableInfoDO.getTableComment());
        objectMap.put("columns", resTableColumn);
        objectMap.put("tableName", tableName);
        //转小写别名
        String tableAliasName = className.substring(0, 1).toLowerCase() + className.substring(1);
        objectMap.put("tableAliasName", tableAliasName);
        objectMap.put("apiName", tableName.replace("_", "/"));
        objectMap.put("baseEntity", objectMap.get("packageName") + ".infrastructure.utils.mp");
        objectMap.put("hasBigDecimal", hasBigDecimal.get());
        objectMap.put("hasLocalDate", hasLocalDate.get());
        objectMap.put("hasLocalDateTime", hasLocalDateTime.get());
        return objectMap;
    }

    /**
     * 生成代码
     *
     * @param projectRootPath 绝对路径
     * @param className       类名
     * @param objectMap       数据
     */
    private void createCode(String projectRootPath, String className, Map<String, Object> objectMap) {
        super.writeFile(new File(projectRootPath + "/application/assembler", className + "Assembler.java"), "assembler.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/domain/entity", className + ".java"), "Entity.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/interfaces/dto", className + "DTO.java"), "DTO.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/interfaces/dto", className + "QueryDTO.java"), "QueryDTO.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/interfaces/vo", className + "VO.java"), "VO.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/domain/repository/po", className + "PO.java"), "PO.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/application/service", className + "AppService.java"), "AppService.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/domain/repository", "I" + className + "DomainRepository.java"), "DomainRepository.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/interfaces/controller", className + "Controller.java"), "Controller.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/infrastructure/repository/mapper", className + "Mapper.java"), "Mapper.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/infrastructure/repository/mapper", className + "Mapper.xml"), "Mapper.xml.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/domain/service", className + "DomainService.java"), "DomainService.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/infrastructure/repository/service", className + "ServiceImpl.java"), "ServiceImpl.java.ftl", objectMap);
        super.writeFile(new File(projectRootPath + "/domain/entity/enums", "TemplateEnum.java"), "TemplateEnum.java.ftl", objectMap);
    }

    /**
     * 获取配置信息
     *
     * @return
     */
    public static Configuration getConfig() {
        try {
            return new PropertiesConfiguration("generator.properties");
        } catch (ConfigurationException e) {
            throw new RuntimeException("获取配置文件失败，", e);
        }
    }


}
