package com.easy.generator.infrastructure.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.easy.generator.domain.repository.ITableInfoDomainRepository;
import com.easy.generator.infrastructure.mapper.TableInfoMapper;
import com.easy.generator.interfaces.dto.TableInfoPageDTO;
import com.easy.generator.domain.entity.TableInfoDO;
import org.springframework.stereotype.Service;

/**
 * com.five.generator.infrastructure.service.impl
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
@Service
public class TableInfoRepositoryImpl extends ServiceImpl<TableInfoMapper, TableInfoDO> implements ITableInfoDomainRepository {


    @Override
    public IPage<TableInfoDO> queryTables(TableInfoPageDTO tableInfoPageDTO) {
        Page<TableInfoPageDTO> page = new Page<>(tableInfoPageDTO.getCurrent(), tableInfoPageDTO.getSize());
        return this.baseMapper.queryTables(page, tableInfoPageDTO);
    }

    @Override
    public TableInfoDO queryTableInfo(String tableName) {
        return this.baseMapper.queryTableInfo(tableName);
    }
}
