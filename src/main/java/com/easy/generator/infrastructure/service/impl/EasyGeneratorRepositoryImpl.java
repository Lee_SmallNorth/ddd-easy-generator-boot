package com.easy.generator.infrastructure.service.impl;


import com.easy.generator.application.constant.AppConstant;
import com.easy.generator.domain.repository.IEasyGeneratorDomainRepository;
import com.easy.generator.infrastructure.utils.ConvertClassNameUtils;
import freemarker.template.TemplateException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

/**
 * com.five.code.domain.service
 *
 * @author smallNorth_Lee
 * @date 2021/12/23
 */
@Service
@AllArgsConstructor
public class EasyGeneratorRepositoryImpl implements IEasyGeneratorDomainRepository {

    private final GeneratorApplicationImpl generatorApplication;

    private final GenerationPackageInfoImpl generatorPackageInfo;

    private final GeneratorPomImpl generatorPom;

    private final GeneratorYmlImpl generatorYml;

    private final GeneratorTestImpl generatorTest;

    private final GenerationJavaClassImpl generationJavaClass;

    @Override
    public void generator(Map<String, Object> objectMap) throws TemplateException, IOException {
        String projectsRoot = objectMap.get("projectRootDir").toString();
        //项目包路径
        String packagePath = objectMap.get("packageName").toString();
        //生成项目路径
        String projectPath = packagePath.replace(".", "/");
        //项目名称
        String artifactName = objectMap.get("artifactId").toString();
        //项目文件路径
        String projectRootPath = String.format("%s%s%s%s/", projectsRoot, artifactName, AppConstant.PROJECT_CHILDREN_PATH, projectPath);
        //class类名
        String className = ConvertClassNameUtils.getClassName(artifactName);
        objectMap.put("package", packagePath);
        objectMap.put("className", className);
        objectMap.put("projectPath", projectPath);

        // 1. 项目初始化
        generatorPackageInfo.doGeneration(objectMap, projectRootPath);

        // 2. 创建  Application.java
        generatorApplication.doGeneration(objectMap, projectRootPath);

        // 3. 创建 application.yml
        generatorYml.doGeneration(objectMap, String.format("%s%s%s", projectsRoot, artifactName, AppConstant.PROJECT_RESOURCE_PATH));

        // 4. 创建 pom.xml
        generatorPom.doGeneration(objectMap, String.format("%s%s/", projectsRoot, artifactName));

        // 5. 创建测试类
        generatorTest.doGeneration(objectMap, String.format("%s%s%s%s/", projectsRoot, artifactName, AppConstant.PROJECT_TEST_PATH, projectPath));

        // 6. 生成class
        generationJavaClass.doGeneration(objectMap, projectRootPath, "");
    }


}
