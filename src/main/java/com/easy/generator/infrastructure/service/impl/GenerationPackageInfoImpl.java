package com.easy.generator.infrastructure.service.impl;

import com.easy.generator.infrastructure.service.BaseService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

/**
 * 项目文件初始化
 *
 * @author smallNorth_Lee
 * @date 2022/1/11
 */
@Service
public class GenerationPackageInfoImpl extends BaseService {

    /**
     * @param objectMap       项目信息
     * @param projectRootPath 项目根路径
     */
    public void doGeneration(Map<String, Object> objectMap, String projectRootPath) {

        String applicationPath = projectRootPath + "application/";
        String domainPath = projectRootPath + "domain/";
        String infrastructurePath = projectRootPath + "infrastructure/";
        String interfacesPath = projectRootPath + "interfaces/";
        String packagePath = objectMap.get("packageName").toString().replace("\\.", "/");

        // 生成应用层 application
        objectMap.put("packageName", packagePath + ".application");
        super.writeFile(new File(applicationPath, "package-info.java"), "package-info.ftl", objectMap);

        objectMap.put("packageName", packagePath + ".constant");
        super.writeFile(new File(applicationPath + "constant/", ""), "", objectMap);

        objectMap.put("packageName", packagePath + ".service");
        super.writeFile(new File(applicationPath + "service/", ""), "", objectMap);

        objectMap.put("packageName", packagePath + ".assembler");
        super.writeFile(new File(applicationPath + "assembler/", ""), "", objectMap);


        //生成领域层 domain
        objectMap.put("packageName", packagePath + ".domain");
        super.writeFile(new File(domainPath, "package-info.java"), "package-info.ftl", objectMap);

        objectMap.put("packageName", packagePath + ".entity");
        super.writeFile(new File(domainPath + "entity/", ""), "", objectMap);

        objectMap.put("packageName", packagePath + ".service");
        super.writeFile(new File(domainPath + "service/", ""), "", objectMap);

        objectMap.put("packageName", packagePath + ".repository");
        super.writeFile(new File(domainPath + "repository/", ""), "", objectMap);

        //生成基础设施层 infrastructure
        objectMap.put("packageName", packagePath + ".infrastructure");
        super.writeFile(new File(infrastructurePath, "package-info.java"), "package-info.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".infrastructure.utils.common");
        super.writeFile(new File(infrastructurePath + "utils/common", "Result.java"), "Result.java.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".infrastructure.utils.common");
        super.writeFile(new File(infrastructurePath + "utils/common", "BeanUtil.java"), "BeanUtil.java.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".infrastructure.utils.common");
        super.writeFile(new File(infrastructurePath + "utils/common", "LogAspect.java"), "LogAspect.java.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".infrastructure.utils.common");
        super.writeFile(new File(infrastructurePath + "utils/common", "Query.java"), "Query.java.ftl",
                objectMap);

//        objectMap.put("packageName", packagePath + ".infrastructure.utils.exception");
//        super.writeFile(new File(infrastructurePath + "utils/exception", "BaseEntity.java"), "baseEntity.java.ftl",
//                objectMap);

        objectMap.put("packageName", packagePath + ".infrastructure.utils.mp");
        super.writeFile(new File(infrastructurePath + "utils/mp", "BaseEntity.java"), "baseEntity.java.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".infrastructure.utils.mp");
        super.writeFile(new File(infrastructurePath + "utils/mp", "MyMetaObjectHandler.java"), "mpMetaHandler.java.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".infrastructure.config");
        super.writeFile(new File(infrastructurePath + "config/", "MybatisPlusConfig.java"), "mpconfig.java.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".infrastructure.config");
        super.writeFile(new File(infrastructurePath + "config/", "Knife4jConfiguration.java"), "Knife4jConfiguration.java.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".infrastructure.config");
        super.writeFile(new File(infrastructurePath + "config/", "CorsConfiguration.java"), "CorsConfiguration.java.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".repository.mapper");
        super.writeFile(new File(infrastructurePath + "repository/mapper", ""), "",
                objectMap);

        objectMap.put("packageName", packagePath + ".repository.impl");
        super.writeFile(new File(infrastructurePath + "repository/service/", ""), "",
                objectMap);

        //生成接口层
        objectMap.put("packageName", packagePath + ".interfaces");
        super.writeFile(new File(interfacesPath, "package-info.java"), "package-info.ftl",
                objectMap);

        objectMap.put("packageName", packagePath + ".controller");
        super.writeFile(new File(interfacesPath + "controller/", ""), "",objectMap);

        objectMap.put("packageName", packagePath + ".dto");
        super.writeFile(new File(interfacesPath + "dto/", ""), "",objectMap);

        objectMap.put("packageName", packagePath + ".vo");
        super.writeFile(new File(interfacesPath + "vo/", ""), "",objectMap);
    }
}
