package com.easy.generator.infrastructure.service.impl;

import com.easy.generator.infrastructure.service.BaseService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

/**
 * 生成启动类
 *
 * @author smallNorth_Lee
 * @date 2022/1/11
 */
@Service
public class GeneratorApplicationImpl extends BaseService {

    /**
     * 生成启动类文件
     *
     * @param ftlMap 变量值
     * @param projectRootPath 项目文件路径
     */
    public void doGeneration(Map<String, Object> ftlMap, String projectRootPath) {

        String className = ftlMap.get("className") + "Application";
        super.writeFile(new File(projectRootPath, className + ".java"), "application.java.ftl", ftlMap);
    }


}
