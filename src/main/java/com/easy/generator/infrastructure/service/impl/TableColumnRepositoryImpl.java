package com.easy.generator.infrastructure.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.easy.generator.domain.entity.TableColumnDO;
import com.easy.generator.domain.repository.ITableColumnDomainRepository;
import com.easy.generator.infrastructure.mapper.TableColumnMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * com.five.generator.infrastructure.service.impl
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
@Service
public class TableColumnRepositoryImpl extends ServiceImpl<TableColumnMapper, TableColumnDO> implements ITableColumnDomainRepository {

    @Override
    public List<TableColumnDO> queryTableColumn(String tableName) {
        return this.baseMapper.queryColumns(tableName);
    }
}
