package com.easy.generator.infrastructure.service.impl;

import com.easy.generator.infrastructure.service.BaseService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

/**
 * 生成pom文件 基于start.aliyun.com
 *
 * @author smallNorth_Lee
 * @date 2022/1/11
 */
@Service
public class GeneratorPomImpl extends BaseService {

    /**
     * 生成pom文件
     * 生成Dockerfile
     *
     * @param objectMap       项目配置信息
     * @param projectRootPath 项目文件路径
     */
    public void doGeneration(Map<String, Object> objectMap, String projectRootPath) {

        // 生成pom.xml
        super.writeFile(new File(projectRootPath, "pom.xml"), "pom.ftl", objectMap);

        // 生成Dockerfile
        super.writeFile(new File(projectRootPath, "Dockerfile"), "dockerfile.ftl", objectMap);

        // 生成.gitignore
        super.writeFile(new File(projectRootPath, ".gitignore"), "gitignore.ftl", objectMap);
    }
}
