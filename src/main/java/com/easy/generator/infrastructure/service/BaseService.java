package com.easy.generator.infrastructure.service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;

/**
 * com.five.generator.domain.service
 *
 * @author smallNorth_Lee
 * @date 2022/1/11
 */
public class BaseService {

    private static Configuration cfg;

    static {
        try {
            cfg = new Configuration(Configuration.VERSION_2_3_23);
            File file = new File(System.getProperty("user.dir") + "/src/main/resources/templates");
            cfg.setDirectoryForTemplateLoading(file);
            cfg.setDefaultEncoding("UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 编码格式化
     *
     * @param ftl 模版
     * @return Template
     * @throws IOException
     */
    protected Template getTemplate(String ftl) throws IOException {
        return cfg.getTemplate(ftl, StandardCharsets.UTF_8.name());
    }

    /**
     * 生成文件
     *
     * @param file      文件
     * @param ftl       模版
     * @param dataModel 数据
     * @throws Exception
     */
    protected void writeFile(File file, String ftl, Object dataModel) {
        if (StringUtils.isEmpty(ftl)) {
            if (!file.exists()) {
                file.mkdirs();
            }
        } else {
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            try {
                file.createNewFile();
                try (OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(file))) {
                    try {
                        getTemplate(ftl).process(dataModel, outputStreamWriter);
                    } finally {
                        outputStreamWriter.flush();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
