package com.easy.generator.infrastructure.service.impl;

import com.easy.generator.infrastructure.service.BaseService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Map;

/**
 * 生成配置文件
 *
 * @author smallNorth_Lee
 * @date 2022/1/11
 */
@Service
public class GeneratorYmlImpl extends BaseService {

    /**
     * 生成配置文件
     *
     * @param objectMap       项目配置信息
     * @param projectRootPath 项目文件路径
     */
    public void doGeneration(Map<String, Object> objectMap, String projectRootPath) {

        //生成xml
        super.writeFile(new File(projectRootPath + "mapper"), "", "");

        //静态资源
        super.writeFile(new File(projectRootPath + "static"), "", "");
        //静态模版
        super.writeFile(new File(projectRootPath + "templates"), "", "");
        super.writeFile(new File(projectRootPath, "application.yml"), "applicationYml.ftl", objectMap);
    }


}
