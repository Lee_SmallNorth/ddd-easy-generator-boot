package com.easy.generator.infrastructure.utils;

/**
 * com.five.generator.infrastructure.utils
 *
 * @author smallNorth_Lee
 * @date 2022/1/13
 */
public class ConvertTableClassUtils {

    public static String getClassName(String str) {
        //className,大小写转换格式
        StringBuilder sbClassName = new StringBuilder();
        if (str.contains("_")) {
            String[] split = str.split("_");
            for (String name : split) {
                sbClassName.append(name.substring(0, 1).toUpperCase()).append(name.substring(1));
            }
            return sbClassName.toString();
        }
        return str;
    }
}
