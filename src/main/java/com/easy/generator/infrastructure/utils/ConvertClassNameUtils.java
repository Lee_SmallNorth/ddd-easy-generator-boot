package com.easy.generator.infrastructure.utils;

/**
 * com.five.generator.infrastructure.utils
 *
 * @author smallNorth_Lee
 * @date 2022/1/13
 */
public class ConvertClassNameUtils {

    public static String getClassName(String artifactId) {
        //className,大小写转换格式
        StringBuilder sbClassName = new StringBuilder();
        if (artifactId.contains("-")) {
            String[] split = artifactId.split("-");
            for (String name : split) {
                sbClassName.append(name.substring(0, 1).toUpperCase()).append(name.substring(1));
            }
            return sbClassName.toString();
        }
        return artifactId;
    }
}
