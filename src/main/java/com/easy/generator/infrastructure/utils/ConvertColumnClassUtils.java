package com.easy.generator.infrastructure.utils;

/**
 * com.five.generator.infrastructure.utils
 *
 * @author smallNorth_Lee
 * @date 2022/1/13
 */
public class ConvertColumnClassUtils {

    public static String getColumnName(String str) {
        //className,大小写转换格式
        StringBuilder sbColumnName = new StringBuilder();
        if (str.contains("_")) {
            String[] split = str.split("_");
            sbColumnName.append(split[0]);
            for (int i = 1; i < split.length; i++) {
                sbColumnName.append(split[i].substring(0, 1).toUpperCase()).append(split[i].substring(1));
            }
            return sbColumnName.toString();
        }
        return str;
    }
}
