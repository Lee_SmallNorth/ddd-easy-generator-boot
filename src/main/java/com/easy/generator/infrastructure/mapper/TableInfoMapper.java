package com.easy.generator.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.easy.generator.interfaces.dto.TableInfoPageDTO;
import com.easy.generator.domain.entity.TableInfoDO;
import org.apache.ibatis.annotations.Param;

/**
 * com.five.generator.infrastructure.mapper
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
public interface TableInfoMapper extends BaseMapper<TableInfoDO> {

    /**
     * 查询所有表
     *
     * @param page             分页
     * @param tableInfoPageDTO 参数
     * @return
     */
    IPage<TableInfoDO> queryTables(Page<TableInfoPageDTO> page, @Param("tableInfoPageDTO") TableInfoPageDTO tableInfoPageDTO);

    /**
     * 查询表明细
     *
     * @param tableName 表名
     * @return
     */
    TableInfoDO queryTableInfo(@Param("tableName") String tableName);
}
