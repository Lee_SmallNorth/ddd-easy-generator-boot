package com.easy.generator.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easy.generator.domain.entity.TableColumnDO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * com.five.generator.infrastructure.mapper
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
public interface TableColumnMapper extends BaseMapper<TableColumnDO> {

    /**
     * 查询表中字段
     *
     * @param tableName 表名
     * @return
     */
    List<TableColumnDO> queryColumns(@Param("tableName") String tableName);
}
