package com.easy.generator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author smallNorth_Lee
 * @date 2022/02/21
 */
@MapperScan("com.easy.generator.**.mapper")
@SpringBootApplication
public class EasyGeneratorBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyGeneratorBootApplication.class, args);
    }

}
