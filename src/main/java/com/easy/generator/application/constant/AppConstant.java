package com.easy.generator.application.constant;

/**
 * 常量
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
public interface AppConstant {

    /**
     * 子路径
     */
    String PROJECT_CHILDREN_PATH = "/src/main/java/";
    /**
     * 静态资源路径
     */
    String PROJECT_RESOURCE_PATH = "/src/main/resources/";

    /**
     * 测试类地址
     */
    String PROJECT_TEST_PATH = "/src/test/java/";

    /**
     * 公共字段,默认不生成，如需自行添加
     */
    String COMMON_FIELD = "id,create_user,create_time,update_user,update_time,is_deleted,tenant_id";


}
