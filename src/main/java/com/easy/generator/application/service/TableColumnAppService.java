package com.easy.generator.application.service;

import com.easy.generator.domain.entity.TableColumnDO;
import com.easy.generator.domain.repository.ITableColumnDomainRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * com.five.generator.application.service
 *
 * @author smallNorth_Lee
 * @date 2022/1/13
 */
@Service
@AllArgsConstructor
public class TableColumnAppService {

    private final ITableColumnDomainRepository tableColumnDomainService;


    /**
     * 查询表下所有字段
     *
     * @param tableName 表名
     * @return
     */
    public List<TableColumnDO> queryTableColumn(String tableName) {
        return tableColumnDomainService.queryTableColumn(tableName);
    }
}
