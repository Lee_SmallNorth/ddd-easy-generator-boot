package com.easy.generator.application.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.easy.generator.domain.repository.IEasyGeneratorDomainRepository;
import com.easy.generator.domain.repository.ITableInfoDomainRepository;
import com.easy.generator.interfaces.dto.TableInfoPageDTO;
import com.easy.generator.domain.entity.TableInfoDO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * com.five.generator.application.service
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
@Service
@AllArgsConstructor
public class TableInfoAppService {

    private final ITableInfoDomainRepository tableInfoDomainService;

    private IEasyGeneratorDomainRepository codeGeneratorService;

    /**
     * 查询所有表
     *
     * @param tableInfoPageDTO 查询参数
     * @return
     */
    public IPage<TableInfoDO> queryTableInfo(TableInfoPageDTO tableInfoPageDTO) {
        return tableInfoDomainService.queryTables(tableInfoPageDTO);
    }
}
