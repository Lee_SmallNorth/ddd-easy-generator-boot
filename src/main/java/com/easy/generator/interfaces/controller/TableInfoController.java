package com.easy.generator.interfaces.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.easy.generator.application.service.TableInfoAppService;
import com.easy.generator.domain.entity.TableInfoDO;
import com.easy.generator.interfaces.dto.TableInfoPageDTO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * com.five.generator.interfaces.controller
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
@AllArgsConstructor
@RestController
@RequestMapping(value = "/table/info")
public class TableInfoController {

    private final TableInfoAppService tableInfoAppService;

    @PostMapping
    public IPage<TableInfoDO> queryTableInfo(@RequestBody TableInfoPageDTO tableInfoPageDTO) {
        return tableInfoAppService.queryTableInfo(tableInfoPageDTO);
    }
}
