package com.easy.generator.interfaces.controller;

import com.easy.generator.application.service.TableColumnAppService;
import com.easy.generator.domain.entity.TableColumnDO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * com.five.generator.interfaces.controller
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
@AllArgsConstructor
@RestController
@RequestMapping(value = "/table/column")
public class TableColumnController {

    private final TableColumnAppService tableColumnAppService;

    @GetMapping("/{tableName}")
    public List<TableColumnDO> queryTableColumn(@PathVariable String tableName) {
        return tableColumnAppService.queryTableColumn(tableName);
    }
}
