package com.easy.generator.interfaces.dto;

import lombok.Data;

/**
 * 分页查询参数
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
@Data
public class TableInfoPageDTO {

    /**
     * 当前页
     */
    private Integer current;

    /**
     * 页数
     */
    private Integer size;

    /**
     * 表名
     */
    private String tableName;
}
