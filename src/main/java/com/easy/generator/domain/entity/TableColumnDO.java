package com.easy.generator.domain.entity;

import com.easy.generator.domain.entity.enums.ColumnTypeEnum;
import lombok.Data;

/**
 * 列名
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
@Data
public class TableColumnDO {

    /**
     * 列名
     */
    private String columnName;

    /**
     * 类型
     */
    private String dataType;

    /**
     * 列描述
     */
    private String columnComment;

    /**
     * 值对象赋值
     */
    public void setDataType() {
        String columnType = ColumnTypeEnum.ofColumnType(this.dataType);
        if (null != columnType) {
            this.setDataType(columnType);
        }
    }
}
