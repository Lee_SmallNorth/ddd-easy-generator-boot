package com.easy.generator.domain.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * com.five.generator.domain.entity.enums
 *
 * @author smallNorth_Lee
 * @date 2022/1/14
 */
@Getter
@AllArgsConstructor
public enum ColumnTypeEnum {

    /**
     * 金额类型
     */
    BIG_DECIMAL("BigDecimal", "金额"),

    /**
     * 日期类型
     */
    LOCAL_DATE("LocalDate", "日期类型"),

    /**
     * 时间类型
     */
    LOCAL_DATE_TIME("LocalDateTime", "时间类型");

    /**
     * 字段类型
     */
    private final String columnType;
    /**
     * 字段描述
     */
    private final String columnDesc;

    /**
     * code select value
     *
     * @param columnType
     * @return
     */
    public static String ofColumnType(String columnType) {
        ColumnTypeEnum columnTypeEnum = Arrays.stream(ColumnTypeEnum.values())
                .filter(x -> x.getColumnType().equals(columnType)).findAny().orElse(null);
        if (null != columnTypeEnum) {
            return columnTypeEnum.getColumnType();
        }
        return null;
    }


}
