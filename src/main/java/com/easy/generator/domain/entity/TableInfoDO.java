package com.easy.generator.domain.entity;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * com.five.generator.domain.entity
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
@Data
public class TableInfoDO {

    /**
     * 表名
     */
    private String tableName;

    /**
     * 表描述
     */
    private String tableComment;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
