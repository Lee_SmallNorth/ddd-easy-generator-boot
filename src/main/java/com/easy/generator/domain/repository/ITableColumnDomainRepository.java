package com.easy.generator.domain.repository;

import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.generator.domain.entity.TableColumnDO;

import java.util.List;

/**
 * com.five.generator.domain.service
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
public interface ITableColumnDomainRepository extends IService<TableColumnDO> {

    /**
     * 查询表下所有字段
     *
     * @param tableName 表名
     * @return
     */
    List<TableColumnDO> queryTableColumn(String tableName);
}
