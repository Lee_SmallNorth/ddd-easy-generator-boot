package com.easy.generator.domain.repository;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.easy.generator.interfaces.dto.TableInfoPageDTO;
import com.easy.generator.domain.entity.TableInfoDO;

/**
 * com.five.generator.domain.service
 *
 * @author smallNorth_Lee
 * @date 2022/1/12
 */
public interface ITableInfoDomainRepository extends IService<TableInfoDO> {

    /**
     * 查询所有表
     *
     * @param tableInfoPageDTO 查询参数
     * @return
     */
    IPage<TableInfoDO> queryTables(TableInfoPageDTO tableInfoPageDTO);

    /**
     * 查询表明细
     *
     * @param tableName 表名
     * @return
     */
    TableInfoDO queryTableInfo(String tableName);
}
