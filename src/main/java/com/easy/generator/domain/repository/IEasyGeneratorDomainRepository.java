package com.easy.generator.domain.repository;

import freemarker.template.TemplateException;

import java.io.IOException;
import java.util.Map;

/**
 * com.five.generator.application.service
 *
 * @author smallNorth_Lee
 * @date 2022/1/11
 */
public interface IEasyGeneratorDomainRepository {

    /**
     * 代码生成
     *
     * @param objectMap 参数
     * @throws TemplateException
     * @throws IOException
     */
    void generator(Map<String, Object> objectMap) throws TemplateException, IOException;
}
